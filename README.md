# Landing Page Movies

### Stack
- HTML 5
- SASS - WEBPACK
- JavaScript

## Preview

![](Desktop.jpg)

![](Mobile.jpg)

# webpack-starter
npm i
npm start
npm run build

## Authors

👤 **Alejandro Galvis <galvis.alejandro@gmail.com>**
- Gitlab: [@Alejo_55](https://gitlab.com/Alejo_55)

