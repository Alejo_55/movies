

//Slider
new Splide( '.splide' ).mount(); 

//Data modal
const dataModal = (id, cual) => {
 
  if(cual == "button--primary"){
    document.getElementById('mainContentModal').classList.add("modalVideo");
    fetch('http://www.omdbapi.com/?i='+id+'&apikey=9c7262e0')
    .then((response) => response.json())
    .then((response) => {
      document.getElementById('titleModal').innerHTML = response.Title;

      if(response.imdbID == "tt1727824"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/mP0VHJYFOAU");
      }else if(response.imdbID == "tt1213641"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/PSoRx87OO6k");
      }else if(response.imdbID == "tt0369610"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/RFinNxS5KN4");
      }else if(response.imdbID == "tt1049413"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/GxcMeZDPAFQ");
      }else if(response.imdbID == "tt4154756"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/6ZfuNTqbHE8");
      }else if(response.imdbID == "tt6342284"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/jPEYpryMp2s");
      }else if(response.imdbID == "tt5715066"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/vtFD8R0s22o");
      }else if(response.imdbID == "tt1477834"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/WDkg3h8PCVU");
      }else if(response.imdbID == "tt0458339"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/JerVrbLldXw");
      }else if(response.imdbID == "tt1502397"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/jKCj3XuPG8M");
      }else if(response.imdbID == "tt0451279"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/1Q8fG0TtVAY");
      }else if(response.imdbID == "tt1431045"){
        document.getElementById('videoModal').setAttribute("src", "https://www.youtube.com/embed/0JnRdfiUMa8");
      }
    })

  }else if(cual == "button--secondary"){
    fetch('http://www.omdbapi.com/?i='+id+'&apikey=9c7262e0')
    .then((response) => response.json())
    .then((response) => {
        document.getElementById('titleModal').innerHTML = response.Title;
        document.getElementById('descriptionModal').innerHTML = response.Plot;
        document.getElementById('imgModal').setAttribute("src", response.Poster)
    })
  }

}

//DataMovies
const moviesData = () => {
  let idsMovies = ["tt1727824", "tt4154756", "tt1213641", "tt6342284", "tt1049413", "tt0369610", "tt1431045", "tt5715066", "tt0451279", "tt1502397", "tt0458339", "tt1477834"]

  idsMovies.map((item, i) => { 
    let apiData = 'http://www.omdbapi.com/?i='+item+'&apikey=9c7262e0'
    fetch(apiData)
    .then((response) => response.json())
    .then((response) => {

      console.log(response)

          let moviesData = document.createElement("li");
          let movieInfo = document.createElement("div");
          let movieTitle = document.createElement("h2");
          let movieImage = document.createElement("img");
          let watchNowButton = document.createElement("button");
          let moreInfoButton = document.createElement("button");
          let movieCategory = ""

          moviesData.setAttribute("class", "Movie--item");
          watchNowButton.setAttribute("data-id", response.imdbID);
          moreInfoButton.setAttribute("data-id", response.imdbID);

          if(response.imdbID == "tt0369610" || response.imdbID == "tt1502397"){
            movieCategory = "NR"
          }
          
          if(response.imdbID == "tt4154756" || response.imdbID == "tt1431045" || response.imdbID == "tt0458339"){
            movieCategory = "MP"
          }

          if(response.imdbID == "tt1049413" || response.imdbID == "tt1727824"){
            movieCategory = "MF"
          }

          if(response.imdbID == "tt1213641" || response.imdbID == "tt6342284" || response.imdbID == "tt1477834"){
            movieCategory = "RE"
          }

          if(response.imdbID == "tt0451279" || response.imdbID == "tt5715066"){
            movieCategory = "TR"
          }

          moviesData.setAttribute(
            "data-category",
            movieCategory
          );
          movieTitle.innerHTML = response.Title;
          movieImage.src = response.Poster;
          watchNowButton.innerHTML = "Watch Now";
          moreInfoButton.innerHTML = "More Info";

          movieInfo.classList.add("Movie--info");
          watchNowButton.classList.add("button--primary");
          moreInfoButton.classList.add("button--secondary");

          watchNowButton.setAttribute("data-modal","modal-one");
          moreInfoButton.setAttribute("data-modal","modal-one");

          moviesData.appendChild(movieInfo);
          moviesData.appendChild(movieImage);
          movieInfo.appendChild(movieTitle);
          movieInfo.appendChild(watchNowButton);
          movieInfo.appendChild(moreInfoButton);

          const movieDataItem = document.getElementById("Movies__list--items");
          movieDataItem.appendChild(moviesData);

          //Modal listener
          var modals = document.querySelectorAll("[data-modal]");
          modals.forEach(function (trigger) {
            trigger.addEventListener("click", function (event) {
              event.preventDefault();
              var modal = document.getElementById(trigger.dataset.modal);
              modal.classList.add("open");
              var exits = modal.querySelectorAll(".modal-exit");

              exits.forEach(function (exit) {
                exit.addEventListener("click", function (event) {
                  event.preventDefault();
                  modal.classList.remove("open");

                  document.getElementById('titleModal').innerHTML = "";
                  document.getElementById('descriptionModal').innerHTML = "";
                  document.getElementById('imgModal').setAttribute("src", "")
                  document.getElementById('mainContentModal').classList.remove("modalVideo");
                  document.getElementById('videoModal').setAttribute("src", "");

                });
              });
            });
          });

    })

  })

};

moviesData();

document.addEventListener('click', function (e) {
  if(e.path[0].className == "button--primary" || e.path[0].className == "button--secondary"){
    dataModal(e.path[0].dataset.id, e.path[0].className);
  }

});

